<?php

namespace JT;

use JT\Product\Product as Product;
use JT\Database\Connection as Connection;

require __DIR__ . "/../../backend/headers.php";
require __DIR__ . "/../../backend/autoload.php";

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST)) {
    $type = filter_var($_POST['type'], FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    $SKU = filter_var($_POST['SKU'], FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    $name = filter_var($_POST['name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    $price = filter_var($_POST['price'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    $attributes = $_POST['attributes'];
    $attributesCleaned = [];

    foreach ($attributes as $attribute) {
        $attributeName = filter_var($attribute['attributeName'], FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
        $attributeValue = filter_var($attribute['attributeValue'], FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);

        $attributesCleaned[] = [
            'attributeName' => (string) $attributeName,
            'attributeValue' => (string) $attributeValue
        ];
    }

    $Connection = new Connection();
    $conn = $Connection->getConnection();

    $product = Product::createProduct($conn, $type, $SKU, $name, $price, $attributesCleaned);

    $product->saveToDatabase();
}
