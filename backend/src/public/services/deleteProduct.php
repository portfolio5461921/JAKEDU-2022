<?php

namespace JT;

use JT\Product\Product as Product;
use JT\Database\Connection as Connection;

require __DIR__ . "/../../backend/headers.php";
require __DIR__ . "/../../backend/autoload.php";

$SKU = json_decode(file_get_contents('php://input'), true);

if (!empty($SKU)) {
    $sanitizedSKUs = [];

    foreach ($SKU as $s) {
        array_push($sanitizedSKUs, filter_var($s, FILTER_SANITIZE_FULL_SPECIAL_CHARS));
    }

    $Connection = new Connection();
    $conn = $Connection->getConnection();

    Product::deleteProduct($conn, $sanitizedSKUs);
}
