<?php

namespace JT;

use JT\Product\Product as Product;
use JT\Database\Connection as Connection;

require __DIR__ . "/../../backend/headers.php";
require __DIR__ . "/../../backend/autoload.php";

$Connection = new Connection();
$conn = $Connection->getConnection();

echo json_encode(Product::fetchAllProducts($conn));
