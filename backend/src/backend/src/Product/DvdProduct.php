<?php

namespace JT\Product;

use JT\Product\Product as Product;

class DVDProduct extends Product
{
    protected $conn, $id, $type, $size;

    public function __construct(\PDO $conn, string $type, string $sku, string $name, float $price, array $attributes)
    {
        parent::__construct($sku, $name, $price);
        $this->conn = $conn;
        $this->setType($type);
        $this->setAttributes($attributes);
    }

    public function setID(int $id)
    {
        if (empty($id)) {
            throw new \InvalidArgumentException('ID must not be empty.');
        }

        if ($id < 0) {
            throw new \InvalidArgumentException('ID must be a non-negative integer.');
        }

        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setAttributes(array $attributes)
    {

        $attributeValuesCleaned = [];

        foreach ($attributes as $attribute) {
            if (empty($attribute['attributeValue'])) {
                throw new \InvalidArgumentException('Product attribute value must not be empty.');
            }

            $pattern = '/^\d+(\.\d+)?$/';

            if (!preg_match($pattern, $attribute['attributeValue'])) {
                throw new \InvalidArgumentException('Invalid input format.');
            }

            $attributeValuesCleaned[] = (int) filter_var($attribute['attributeValue'], FILTER_SANITIZE_NUMBER_INT);
        }
        $this->size = $attributeValuesCleaned[0];
    }

    public function getAttributes(): array
    {
        return ['size' => $this->size];
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setType(string $type)
    {
        if (empty($type)) {
            throw new \InvalidArgumentException('Product type must not be empty.');
        }

        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function saveToDatabase(): void
    {
        if (parent::checkDuplicateSKU($this->getSKU())) {
            echo '{"status":"Duplicate SKU detected"}';
            http_response_code(500);
            exit;
        }

        $stmt = $this->conn->prepare("INSERT INTO `products` (`sku`, `price`, `name`, `product_type_id`) VALUES (?,?,?,?);");
        $stmt->execute([$this->getSKU(), $this->getPrice(), $this->getName(), $this->getProductTypeID($this->getType())]);
        $this->setID($this->conn->lastInsertId());

        $stmt = $this->conn->prepare("INSERT INTO `attribute_value_int` (`attribute_id`, `product_id`, `value`) VALUES (?,?,?)");
        $stmt->execute([$this->getAttributeID('size'), $this->getID(), $this->getSize()]);
    }
}
