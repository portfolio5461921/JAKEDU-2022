<?php

namespace JT\Product;

use JT\Database\Connection as Connection;

abstract class Product extends Connection
{
    protected $id, $sku, $name, $price;

    public function __construct(string $sku, string $name, float $price)
    {
        $this->setSKU($sku);
        $this->setName($name);
        $this->setPrice($price);
    }

    public function setID(int $id)
    {
        if (empty($id)) {
            throw new \InvalidArgumentException('ID must not be empty.');
        }

        if ($id < 0) {
            throw new \InvalidArgumentException('ID must be a non-negative integer.');
        }

        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setSKU(string $sku)
    {
        if (empty($sku)) {
            throw new \InvalidArgumentException('Product SKU must not be empty.');
        }

        if (strlen($sku) < 3 || strlen($sku) > 25) {
            throw new \InvalidArgumentException('Product SKU should be between 3 and 25 characters long.');
        }

        if (strpos($sku, ' ') !== false) {
            throw new \InvalidArgumentException('Product SKU must not contain spaces.');
        }

        $this->sku = $sku;
    }

    public function getSKU(): string
    {
        return $this->sku;
    }

    public function setName(string $name)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException('Product name must not be empty.');
        }

        if (strlen($name) < 1 || strlen($name) > 70) {
            throw new \InvalidArgumentException('Product name should be between 1 and 70 characters long.');
        }

        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setPrice(float $price)
    {
        if (empty($price)) {
            throw new \InvalidArgumentException('Product price must not be empty.');
        }

        if (!is_numeric($price) || $price < 0) {
            throw new \Exception("Product price must be a positive number.");
        }

        if ($price > 9999.99) {
            throw new \Exception("Product price must be less than 9999.99");
        }

        $this->price = $price;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    abstract public function saveToDatabase(): void;

    public static function createProduct(\PDO $conn, string $type, string $sku, string $name, float $price, array $attributeValue): Object
    {
        $class = "JT\\Product\\" . ucfirst(strtolower($type)) . "Product";
        if (class_exists($class)) {
            return new $class($conn, $type, $sku, $name, $price, $attributeValue);
        } else {
            throw new \Exception("Invalid product type: $type");
        }
    }

    public static function fetchAllProducts(\PDO $conn): array
    {
        $products = [];
        $stmt = $conn->prepare("SELECT * FROM `products`");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $stmt2 = $conn->prepare("SELECT `name` FROM `product_types` WHERE `id` = ?");
            $stmt2->execute([$row['product_type_id']]);
            $result = $stmt2->fetch(\PDO::FETCH_ASSOC);

            $type = (string) $result['name'];
            $sku = (string) $row['sku'];
            $name = (string) $row['name'];
            $price = (float) $row['price'];

            $product = Product::createProduct($conn, $type, $sku, $name, $price, Product::fetchAttribute($conn, $row['id']));

            array_push($products, ['type' => $product->getType(), 'SKU' => $product->getSKU(), 'name' => $product->getName(), 'price' => $product->getPrice(), 'attributes' => $product->getAttributes()]);
        }
        return $products;
    }

    public static function deleteProduct(\PDO $conn, $sku): void
    {
        foreach ($sku as $i) {
            $stmt = $conn->prepare("DELETE FROM `products` WHERE `sku` = ?");
            $stmt->execute([$i]);
        }
    }


    protected static function fetchAttribute(\PDO $conn, int $productID): array
    {
        $stmt = $conn->prepare("SELECT attribute_id, product_id, value as 'attribute_value' FROM attribute_value_decimal WHERE product_id = :pid
        UNION
        SELECT attribute_id, product_id, value as 'attribute_value' FROM attribute_value_int WHERE product_id = :pid
        UNION
        SELECT attribute_id, product_id, value as 'attribute_value' FROM attribute_value_text WHERE product_id = :pid
        UNION
        SELECT attribute_id, product_id, value as 'attribute_value' FROM attribute_value_varchar WHERE product_id = :pid;");
        $stmt->bindParam(':pid', $productID);
        $stmt->execute();

        $attributes = [];

        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $attributeValue = $row['attribute_value'];
            $stmt2 = $conn->prepare("SELECT `name` FROM `attributes` WHERE `id` = ?");
            $stmt2->execute([$row['attribute_id']]);
            $result = $stmt2->fetch(\PDO::FETCH_ASSOC);
            $attributeName = $result['name'];
            $attributes[] = ['attributeName' => $attributeName, 'attributeValue' => $attributeValue];
        }

        return $attributes;
    }

    protected function getAttributeID(string $attributeName): int
    {
        $stmt = $this->conn->prepare("SELECT id FROM attributes WHERE name = ?");
        $stmt->execute([$attributeName]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result['id'];
    }

    protected function getProductTypeID(string $type): int
    {
        $stmt = $this->conn->prepare("SELECT `id` from `product_types` WHERE `name` = ?");
        $stmt->execute([$type]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result['id'];
    }

    protected function checkDuplicateSKU($sku): bool
    {
        $products = self::fetchAllProducts($this->conn);

        foreach ($products as $product) {
            if ($sku === $product['SKU']) {
                return true;
            }
        }
        return false;
    }
}
