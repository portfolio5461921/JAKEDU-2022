<?php

namespace JT\Database;

class Connection
{
    protected $conn;

    public function __construct()
    {
        $db = parse_ini_file(__DIR__ . "/../../config.ini");

        $user = (string) $db['user'];
        $pass = (string) $db['pass'];
        $name = (string) $db['dbname'];
        $host = (string) $db['host'];
        $type = (string) $db['type'];

        $this->conn = new \PDO("$type:host=$host;dbname=$name", "$user", "$pass");
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function getConnection()
    {
        return $this->conn;
    }
}
