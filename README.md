# Junior Test

Šī lietotne ir tests, kas izveidots kā daļa no Scandiweb Junior Testa.

Tā ietver produktu saraksta un produktu pievienošanas lapas, demonstrējot OOP principu izmantošanu PHP valodā, lai apstrādātu atšķirības produktu tipa loģikā un uzvedībā.

Projekts ietver nozīmīgus klašu paplašinājumus (extend), ar abstraktu klašu galvenā produkta loģikas (polymorphism provision) izmantošanu.

🌐 Lietotne ir pieejama šeit: [Junior Test](https://juniortestpppps.000webhostapp.com/)


## Izmantotās tehnoloģijas

* **Frontend**: React
* **Backend**: PHP
* **Datubāze**: MySQL
* **Konteinerizācija**: Docker


## Funkcijas

* **Produktu saraksta lapa**: Parāda produktu sarakstu, kur tiek attēlota saistītā informācija, piemēram, SKU, nosaukums, cena un atribūti.
* **Produktu pievienošanas lapa:** Ļauj lietotājiem pievienot jaunus produktus, nodrošinot nepieciešamos datus.
* **OOP principi:** Projektā plaši tiek izmantoti PHP objektorientētas programmēšanas principi, lai pārvaldītu produktu tipu loģikas un uzvedības atšķirības.
* **Polimorfisms:** Abstraktās klases izveide galvenajai produktu loģikai ļauj izmantot polimorfismu, nodrošinot elastīgu, atšķirīgu produktu tipu apstrādi.
* **Datu validācija:** Ievades dati tiek rūpīgi validēti un attīrīti, lai nodrošinātu datu integritāti un drošību. Frontend tiek izmantota regex validācija, savukārt backend tiek izmantotas PHP funkcijas, piemēram, `filter_var`, kā arī veiktas vairākas pārbaudes attiecībā uz objektu setteriem un getteriem.


## Konteinerizācija

Lietotne ir konteinerizēta, izmantojot Docker, ar šādiem konteineriem:

1. **Frontend** : Nginx konteiners, kas kalpo React priekšpusē.
2. **Backend** : PHP-Apache konteiners, kas atbild par servera puses loģiku.
3. **Datubāze (DB)** : MySQL konteiners, kas kalpo kā datubāze produktu informācijas uzglabāšanai.
4. **PhpMyAdmin** : Konteiners ar PhpMyAdmin, lai ērti pārvaldītu datubāzi.
