-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: May 24, 2023 at 04:18 PM
-- Server version: 5.7.42
-- PHP Version: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `storeEAV`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`) VALUES
(1, 'Size'),
(2, 'Weight'),
(4, 'Height'),
(5, 'Width'),
(6, 'Length');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_value_decimal`
--

CREATE TABLE `attribute_value_decimal` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_value_decimal`
--

INSERT INTO `attribute_value_decimal` (`id`, `attribute_id`, `product_id`, `value`) VALUES
(16, 2, 175, 7),
(17, 2, 176, 42);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_value_int`
--

CREATE TABLE `attribute_value_int` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_value_int`
--

INSERT INTO `attribute_value_int` (`id`, `attribute_id`, `product_id`, `value`) VALUES
(124, 1, 173, 700),
(125, 1, 174, 400),
(126, 4, 177, 52),
(127, 5, 177, 12),
(128, 6, 177, 74),
(129, 4, 178, 65),
(130, 5, 178, 42),
(131, 6, 178, 37);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_value_text`
--

CREATE TABLE `attribute_value_text` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_value_varchar`
--

CREATE TABLE `attribute_value_varchar` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `name` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `price`, `name`, `product_type_id`) VALUES
(173, '019ABC', 19.99, 'Acme Disc', 2),
(174, '148DEF', 9.99, 'DVD Disc', 2),
(175, 'BK9924', 14.92, 'War And Peace', 1),
(176, 'AW812', 948.00, 'Alice Whitehead', 1),
(177, 'TAB192', 762.00, 'Table', 3),
(178, 'CH824', 42.00, 'Chair', 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `name`) VALUES
(1, 'Book'),
(2, 'DVD'),
(3, 'Furniture');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_value_decimal`
--
ALTER TABLE `attribute_value_decimal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `autoremove` (`product_id`);

--
-- Indexes for table `attribute_value_int`
--
ALTER TABLE `attribute_value_int`
  ADD PRIMARY KEY (`id`),
  ADD KEY `autoremove2` (`product_id`);

--
-- Indexes for table `attribute_value_text`
--
ALTER TABLE `attribute_value_text`
  ADD PRIMARY KEY (`id`),
  ADD KEY `autoremove3` (`product_id`);

--
-- Indexes for table `attribute_value_varchar`
--
ALTER TABLE `attribute_value_varchar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `autoremove4` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `attribute_value_decimal`
--
ALTER TABLE `attribute_value_decimal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `attribute_value_int`
--
ALTER TABLE `attribute_value_int`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `attribute_value_text`
--
ALTER TABLE `attribute_value_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attribute_value_varchar`
--
ALTER TABLE `attribute_value_varchar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attribute_value_decimal`
--
ALTER TABLE `attribute_value_decimal`
  ADD CONSTRAINT `autoremove` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attribute_value_int`
--
ALTER TABLE `attribute_value_int`
  ADD CONSTRAINT `autoremove2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attribute_value_text`
--
ALTER TABLE `attribute_value_text`
  ADD CONSTRAINT `autoremove3` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attribute_value_varchar`
--
ALTER TABLE `attribute_value_varchar`
  ADD CONSTRAINT `autoremove4` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;